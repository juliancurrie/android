package cs.mad.flashcards.models

import androidx.room.*

data class FlashcardSetContainer(
    val flashcardSets: List<FlashcardSet>
)

@Entity
data class FlashcardSet (
    @PrimaryKey var title: String

)

@Dao
interface FlashcardSetDao {
    @Query("select * from FlashcardSet")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(set: FlashcardSet)

    @Insert
    suspend fun insertAll(sets: List<FlashcardSet>)

    @Update
    suspend fun update(set: FlashcardSet)

    @Delete
    suspend fun delete(set: FlashcardSet)

    @Query("delete from FlashcardSet")
    suspend fun deleteAll()
}
