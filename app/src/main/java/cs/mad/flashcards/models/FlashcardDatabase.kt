package cs.mad.flashcards.models

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Flashcard::class], version = 1)
abstract class FlashcardDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "Flashcard_DATABASE"
    }

    abstract fun flashcardDao() : FlashcardDao
}