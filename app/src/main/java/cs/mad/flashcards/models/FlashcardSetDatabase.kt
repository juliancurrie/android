package cs.mad.flashcards.models

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FlashcardSet::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FlashcardSet_DATABASE"
    }

    abstract fun flashcardSetDao() : FlashcardSetDao
}