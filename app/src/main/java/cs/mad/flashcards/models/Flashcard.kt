package cs.mad.flashcards.models

import androidx.room.*

data class FlashcardContainer(
    val flashcards: List<Flashcard>
)

@Entity
data class Flashcard (
    @PrimaryKey var term: String,
    val definition: String,
)

@Dao
interface FlashcardDao {
    @Query("select * from Flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(card: Flashcard)

    @Insert
    suspend fun insertAll(cards: List<Flashcard>)

    @Update
    suspend fun update(card: Flashcard)

    @Delete
    suspend fun delete(card: Flashcard)

    @Query("delete from Flashcard")
    suspend fun deleteAll()
}
