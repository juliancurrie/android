package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.models.FlashcardSet
import cs.mad.flashcards.models.FlashcardSetDao
import cs.mad.flashcards.models.FlashcardSetDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var flashcardSetDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java, FlashcardSetDatabase.databaseName
        ).build()
        flashcardSetDao = db.flashcardSetDao()
        binding.flashcardSetList.adapter = FlashcardSetAdapter(flashcardSetDao)

        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("Test"))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
            //clearStorage()
            loadData()
        }
    }

    private fun loadData() {
        runOnIO {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(flashcardSetDao.getAll())
        }
    }

    private fun clearStorage() {
        runOnIO {
            flashcardSetDao.deleteAll()
        }
    }

    fun runOnIO(lambda: suspend () -> Unit) {
        runBlocking {
            launch(Dispatchers.IO) { lambda() }
        }
    }
}
